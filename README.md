# ClassroomTimers

A collection of web based timers for use in classroom settings created with
HTML5, CSS3, and JavaScript.  Nora is a contributor, so is Brigid. Peter holds
the keys to the realm, and guards it fiercely from the beginner mistakes the
rest of us are making.

With this project we are honing our front-end web skills while, equally if not
more importantly, beginning to learn git workflow.
